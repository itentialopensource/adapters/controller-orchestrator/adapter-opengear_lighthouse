# Opengear Lighthouse

Vendor: Opengear
Homepage: https://opengear.com/

Product: Lighthouse
Product Page: https://opengear.com/products/lighthouse/

## Introduction
We classify Opengear Lighthouse into the Data Center and Network Services domains as Lighthouse provides visiblity and control for distributed networks.

## Why Integrate
The Opengear Lighthouse adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Opengear Lighthouse. With this adapter you have the ability to perform operations on items such as:

- Managed Devices
- Nodes
- Ports
- Interfaces
- Services
- Templates

## Additional Product Documentation
[Opengear Lighthouse API Specification](https://ftp.opengear.com/download/lighthouse_software/all/20.Q2.0/doc/og-rest-api-specification-v3-5.html)