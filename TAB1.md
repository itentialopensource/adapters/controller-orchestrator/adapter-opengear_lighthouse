# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Opengear_lighthouse System. The API that was used to build the adapter for Opengear_lighthouse is usually available in the report directory of this adapter. The adapter utilizes the Opengear_lighthouse API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Opengear Lighthouse adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Opengear Lighthouse. With this adapter you have the ability to perform operations on items such as:

- Managed Devices
- Nodes
- Ports
- Interfaces
- Services
- Templates

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
