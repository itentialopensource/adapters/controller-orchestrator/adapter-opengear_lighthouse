
## 0.2.4 [10-14-2024]

* Changes made at 2024.10.14_18:37PM

See merge request itentialopensource/adapters/adapter-opengear_lighthouse!13

---

## 0.2.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-opengear_lighthouse!11

---

## 0.2.2 [08-14-2024]

* Changes made at 2024.08.14_17:18PM

See merge request itentialopensource/adapters/adapter-opengear_lighthouse!10

---

## 0.2.1 [08-07-2024]

* Changes made at 2024.08.06_18:17PM

See merge request itentialopensource/adapters/adapter-opengear_lighthouse!9

---

## 0.2.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!8

---

## 0.1.8 [03-28-2024]

* Changes made at 2024.03.28_13:30PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!7

---

## 0.1.7 [03-11-2024]

* Changes made at 2024.03.11_15:52PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!6

---

## 0.1.6 [02-28-2024]

* Changes made at 2024.02.28_11:17AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!5

---

## 0.1.5 [01-09-2024]

* fix link

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!4

---

## 0.1.4 [01-09-2024]

* fix links

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!3

---

## 0.1.3 [01-05-2024]

* more migration changes

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!2

---

## 0.1.2 [01-04-2024]

* 2023 Adapter Migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-opengear_lighthouse!1

---

## 0.1.1 [01-02-2024]

* Bug fixes and performance improvements

See commit 3b9d6d8

---
