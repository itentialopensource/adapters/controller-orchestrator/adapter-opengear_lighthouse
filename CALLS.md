## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Opengear Lighthouse. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Opengear Lighthouse.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>


### Specific Adapter Calls

Specific adapter calls are built based on the API of the Opengear_lighthouse. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createSession(body, callback)</td>
    <td style="padding:15px">createSession</td>
    <td style="padding:15px">{base_path}/{version}/sessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSession(sessionUid, callback)</td>
    <td style="padding:15px">getSession</td>
    <td style="padding:15px">{base_path}/{version}/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">responseToSessionChallenge(sessionUid, body, callback)</td>
    <td style="padding:15px">responseToSessionChallenge</td>
    <td style="padding:15px">{base_path}/{version}/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSession(sessionUid, callback)</td>
    <td style="padding:15px">deleteSession</td>
    <td style="padding:15px">{base_path}/{version}/sessions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodes(perPage, page, searchparameters, json, jb64, operator, searchId, callback)</td>
    <td style="padding:15px">getNodes</td>
    <td style="padding:15px">{base_path}/{version}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enrollNewNode(body, callback)</td>
    <td style="padding:15px">enrollNewNode</td>
    <td style="padding:15px">{base_path}/{version}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeSmartgroupList(callback)</td>
    <td style="padding:15px">getNodeSmartgroupList</td>
    <td style="padding:15px">{base_path}/{version}/nodes/smartgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNodeSmartgroup(body, callback)</td>
    <td style="padding:15px">createNodeSmartgroup</td>
    <td style="padding:15px">{base_path}/{version}/nodes/smartgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeSmartgroup(groupId, callback)</td>
    <td style="padding:15px">getNodeSmartgroup</td>
    <td style="padding:15px">{base_path}/{version}/nodes/smartgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodeSmartgroup(groupId, callback)</td>
    <td style="padding:15px">deleteNodeSmartgroup</td>
    <td style="padding:15px">{base_path}/{version}/nodes/smartgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNodeSmartgroup(groupId, body, callback)</td>
    <td style="padding:15px">updateNodeSmartgroup</td>
    <td style="padding:15px">{base_path}/{version}/nodes/smartgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmartgroupNodes(groupId, callback)</td>
    <td style="padding:15px">getSmartgroupNodes</td>
    <td style="padding:15px">{base_path}/{version}/nodes/smartgroups/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadManifest(callback)</td>
    <td style="padding:15px">downloadManifest</td>
    <td style="padding:15px">{base_path}/{version}/nodes/manifest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findNodeByID(id, callback)</td>
    <td style="padding:15px">findNodeByID</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNode(id, body, callback)</td>
    <td style="padding:15px">putNode</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodesId(id, callback)</td>
    <td style="padding:15px">DELETE_nodes-id</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeRegistrationPackageByID(id, callback)</td>
    <td style="padding:15px">getNodeRegistrationPackageByID</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/registration_package?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeTags(id, callback)</td>
    <td style="padding:15px">getNodeTags</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNodeTag(id, body, callback)</td>
    <td style="padding:15px">createNodeTag</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesIdTagsTagValueId(tagValueId, id, callback)</td>
    <td style="padding:15px">GET_nodes-id-tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodesIdTagsTagValueId(tagValueId, id, callback)</td>
    <td style="padding:15px">DELETE_nodes-id-tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNodesIdTagsTagValueId(tagValueId, id, body, callback)</td>
    <td style="padding:15px">PUT_nodes-id-tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeCellHealthRuntimeDetails(id, callback)</td>
    <td style="padding:15px">getNodeCellHealthRuntimeDetails</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/cellhealth_runtime_details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getnodesearchfields(perPage, page, searchparameters, json, jb64, operator, searchId, callback)</td>
    <td style="padding:15px">Get node search fields</td>
    <td style="padding:15px">{base_path}/{version}/nodes/ids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getnodesearchfields1(callback)</td>
    <td style="padding:15px">Get node search fields</td>
    <td style="padding:15px">{base_path}/{version}/nodes/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesIdPorts(id, callback)</td>
    <td style="padding:15px">GET_nodes-id-ports</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPorts(perPage, page, searchparameters, json, jb64, operator, searchId, callback)</td>
    <td style="padding:15px">getPorts</td>
    <td style="padding:15px">{base_path}/{version}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortsId(id, callback)</td>
    <td style="padding:15px">GET_ports-id</td>
    <td style="padding:15px">{base_path}/{version}/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortsSmartgroups(callback)</td>
    <td style="padding:15px">GET_ports-smartgroups</td>
    <td style="padding:15px">{base_path}/{version}/ports/smartgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPortsSmartgroups(body, callback)</td>
    <td style="padding:15px">POST_ports-smartgroups</td>
    <td style="padding:15px">{base_path}/{version}/ports/smartgroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortsSmartgroupsId(id, callback)</td>
    <td style="padding:15px">GET_ports-smartgroups-id</td>
    <td style="padding:15px">{base_path}/{version}/ports/smartgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPortsSmartgroupsId(id, body, callback)</td>
    <td style="padding:15px">PUT_ports-smartgroups-id</td>
    <td style="padding:15px">{base_path}/{version}/ports/smartgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortsSmartgroupsId(id, callback)</td>
    <td style="padding:15px">DELETE_ports-smartgroups-id</td>
    <td style="padding:15px">{base_path}/{version}/ports/smartgroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagedDevices(perPage, page, searchparameters, callback)</td>
    <td style="padding:15px">GET_managed_devices</td>
    <td style="padding:15px">{base_path}/{version}/managed_devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchId(json, jb64, searchparameters, operator, callback)</td>
    <td style="padding:15px">getSearchId</td>
    <td style="padding:15px">{base_path}/{version}/search/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchId1(json, jb64, searchparameters, operator, callback)</td>
    <td style="padding:15px">getSearchId1</td>
    <td style="padding:15px">{base_path}/{version}/search/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchId2(json, jb64, searchparameters, operator, callback)</td>
    <td style="padding:15px">getSearchId2</td>
    <td style="padding:15px">{base_path}/{version}/search/managed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesHttps(zip, callback)</td>
    <td style="padding:15px">GET_services-https</td>
    <td style="padding:15px">{base_path}/{version}/services/https?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesHttps(body, callback)</td>
    <td style="padding:15px">PUT_services-https</td>
    <td style="padding:15px">{base_path}/{version}/services/https?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesCellhealth(callback)</td>
    <td style="padding:15px">GET_services-cellhealth</td>
    <td style="padding:15px">{base_path}/{version}/services/cellhealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesCellhealth(body, callback)</td>
    <td style="padding:15px">PUT_services-cellhealth</td>
    <td style="padding:15px">{base_path}/{version}/services/cellhealth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesNtp(callback)</td>
    <td style="padding:15px">GET_services-ntp</td>
    <td style="padding:15px">{base_path}/{version}/services/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesNtp(body, callback)</td>
    <td style="padding:15px">PUT_services-ntp</td>
    <td style="padding:15px">{base_path}/{version}/services/ntp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesConsoleGateway(callback)</td>
    <td style="padding:15px">GET_services-console_gateway</td>
    <td style="padding:15px">{base_path}/{version}/services/console_gateway?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesConsoleGateway(body, callback)</td>
    <td style="padding:15px">PUT_services-console_gateway</td>
    <td style="padding:15px">{base_path}/{version}/services/console_gateway?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesLhvpn(callback)</td>
    <td style="padding:15px">GET_services-lhvpn</td>
    <td style="padding:15px">{base_path}/{version}/services/lhvpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesLhvpn(body, callback)</td>
    <td style="padding:15px">PUT_services-lhvpn</td>
    <td style="padding:15px">{base_path}/{version}/services/lhvpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesSnmp(callback)</td>
    <td style="padding:15px">GET_services-snmp</td>
    <td style="padding:15px">{base_path}/{version}/services/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesSnmp(body, callback)</td>
    <td style="padding:15px">PUT_services-snmp</td>
    <td style="padding:15px">{base_path}/{version}/services/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesSyslog(callback)</td>
    <td style="padding:15px">GET_services-syslog</td>
    <td style="padding:15px">{base_path}/{version}/services/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServicesSyslog(body, callback)</td>
    <td style="padding:15px">POST_services-syslog</td>
    <td style="padding:15px">{base_path}/{version}/services/syslog?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesSyslogSyslogServerId(syslogServerId, callback)</td>
    <td style="padding:15px">GET_services-syslog-syslog_server_id</td>
    <td style="padding:15px">{base_path}/{version}/services/syslog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesSyslogSyslogServerId(syslogServerId, body, callback)</td>
    <td style="padding:15px">PUT_services-syslog-syslog_server_id</td>
    <td style="padding:15px">{base_path}/{version}/services/syslog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicesSyslogSyslogServerId(syslogServerId, callback)</td>
    <td style="padding:15px">DELETE_services-syslog-syslog_server_id</td>
    <td style="padding:15px">{base_path}/{version}/services/syslog/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesSnmpManager(callback)</td>
    <td style="padding:15px">GET_services-snmp_manager</td>
    <td style="padding:15px">{base_path}/{version}/services/snmp_manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServicesSnmpManager(body, callback)</td>
    <td style="padding:15px">PUT_services-snmp_manager</td>
    <td style="padding:15px">{base_path}/{version}/services/snmp_manager?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTagsNodeTags(perPage, page, callback)</td>
    <td style="padding:15px">GET_tags-node_tags</td>
    <td style="padding:15px">{base_path}/{version}/tags/node_tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTagsNodeTags(body, callback)</td>
    <td style="padding:15px">POST_tags-node_tags</td>
    <td style="padding:15px">{base_path}/{version}/tags/node_tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTagsNodeTagsTagValueId(tagValueId, body, callback)</td>
    <td style="padding:15px">PUT_tags-node_tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/tags/node_tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagsNodeTagsTagValueId(tagValueId, callback)</td>
    <td style="padding:15px">DELETE_tags-node_tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/tags/node_tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaces(perPage, page, callback)</td>
    <td style="padding:15px">getInterfaces</td>
    <td style="padding:15px">{base_path}/{version}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfacesId(id, callback)</td>
    <td style="padding:15px">GET_interfaces-id</td>
    <td style="padding:15px">{base_path}/{version}/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putInterfacesId(id, body, callback)</td>
    <td style="padding:15px">PUT_interfaces-id</td>
    <td style="padding:15px">{base_path}/{version}/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemHostname(callback)</td>
    <td style="padding:15px">GET_system-hostname</td>
    <td style="padding:15px">{base_path}/{version}/system/hostname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemHostname(body, callback)</td>
    <td style="padding:15px">PUT_system-hostname</td>
    <td style="padding:15px">{base_path}/{version}/system/hostname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemLighthouseName(callback)</td>
    <td style="padding:15px">GET_system-lighthouse_name</td>
    <td style="padding:15px">{base_path}/{version}/system/lighthouse_name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemWebuiSessionTimeout(callback)</td>
    <td style="padding:15px">GET_system-webui_session_timeout</td>
    <td style="padding:15px">{base_path}/{version}/system/webui_session_timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemWebuiSessionTimeout(body, callback)</td>
    <td style="padding:15px">PUT_system-webui_session_timeout</td>
    <td style="padding:15px">{base_path}/{version}/system/webui_session_timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemCliSessionTimeout(callback)</td>
    <td style="padding:15px">GET_system-cli_session_timeout</td>
    <td style="padding:15px">{base_path}/{version}/system/cli_session_timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemCliSessionTimeout(body, callback)</td>
    <td style="padding:15px">PUT_system-cli_session_timeout</td>
    <td style="padding:15px">{base_path}/{version}/system/cli_session_timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemGlobalEnrollmentToken(callback)</td>
    <td style="padding:15px">GET_system-global_enrollment_token</td>
    <td style="padding:15px">{base_path}/{version}/system/global_enrollment_token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemGlobalEnrollmentToken(body, callback)</td>
    <td style="padding:15px">PUT_system-global_enrollment_token</td>
    <td style="padding:15px">{base_path}/{version}/system/global_enrollment_token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemManifestLink(callback)</td>
    <td style="padding:15px">GET_system-manifest_link</td>
    <td style="padding:15px">{base_path}/{version}/system/manifest_link?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemOsDefaultAddress(callback)</td>
    <td style="padding:15px">GET_system-os_default_address</td>
    <td style="padding:15px">{base_path}/{version}/system/os_default_address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemSshPort(callback)</td>
    <td style="padding:15px">GET_system-ssh_port</td>
    <td style="padding:15px">{base_path}/{version}/system/ssh_port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemSshPort(body, callback)</td>
    <td style="padding:15px">PUT_system-ssh_port</td>
    <td style="padding:15px">{base_path}/{version}/system/ssh_port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemTimezone(callback)</td>
    <td style="padding:15px">GET_system-timezone</td>
    <td style="padding:15px">{base_path}/{version}/system/timezone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemTimezone(body, callback)</td>
    <td style="padding:15px">PUT_system-timezone</td>
    <td style="padding:15px">{base_path}/{version}/system/timezone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemExternalEndpoints(callback)</td>
    <td style="padding:15px">GET_system-external_endpoints</td>
    <td style="padding:15px">{base_path}/{version}/system/external_endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemExternalEndpoints(body, callback)</td>
    <td style="padding:15px">POST_system-external_endpoints</td>
    <td style="padding:15px">{base_path}/{version}/system/external_endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemExternalEndpointsId(id, callback)</td>
    <td style="padding:15px">GET_system-external_endpoints-id</td>
    <td style="padding:15px">{base_path}/{version}/system/external_endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemExternalEndpointsId(id, body, callback)</td>
    <td style="padding:15px">PUT_system-external_endpoints-id</td>
    <td style="padding:15px">{base_path}/{version}/system/external_endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSystemExternalEndpointsId(id, callback)</td>
    <td style="padding:15px">DELETE_system-external_endpoints-id</td>
    <td style="padding:15px">{base_path}/{version}/system/external_endpoints/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemTime(callback)</td>
    <td style="padding:15px">GET_system-time</td>
    <td style="padding:15px">{base_path}/{version}/system/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemTime(body, callback)</td>
    <td style="padding:15px">PUT_system-time</td>
    <td style="padding:15px">{base_path}/{version}/system/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSystemConfig(callback)</td>
    <td style="padding:15px">DELETE_system-config</td>
    <td style="padding:15px">{base_path}/{version}/system/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemFirmwareUpgrade(file, firmwareUrl, firmwareOptions, callback)</td>
    <td style="padding:15px">POST_system-firmware_upgrade</td>
    <td style="padding:15px">{base_path}/{version}/system/firmware_upgrade?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemFirmwareUpgradeStatus(callback)</td>
    <td style="padding:15px">GET_system-firmware_upgrade_status</td>
    <td style="padding:15px">{base_path}/{version}/system/firmware_upgrade_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemEntitlements(callback)</td>
    <td style="padding:15px">GET_system-entitlements</td>
    <td style="padding:15px">{base_path}/{version}/system/entitlements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemLicenses(callback)</td>
    <td style="padding:15px">GET_system-licenses</td>
    <td style="padding:15px">{base_path}/{version}/system/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemLicenses(body, callback)</td>
    <td style="padding:15px">POST_system-licenses</td>
    <td style="padding:15px">{base_path}/{version}/system/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemLicensesFile(file, callback)</td>
    <td style="padding:15px">POST_system-licenses-file</td>
    <td style="padding:15px">{base_path}/{version}/system/licenses/file?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemVersion(callback)</td>
    <td style="padding:15px">GET_system-version</td>
    <td style="padding:15px">{base_path}/{version}/system/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemAlternateApi(callback)</td>
    <td style="padding:15px">GET_system-alternate_api</td>
    <td style="padding:15px">{base_path}/{version}/system/alternate_api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSystemAlternateApi(body, callback)</td>
    <td style="padding:15px">PUT_system-alternate_api</td>
    <td style="padding:15px">{base_path}/{version}/system/alternate_api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemConfigBackup(id, callback)</td>
    <td style="padding:15px">GET_system-config_backup</td>
    <td style="padding:15px">{base_path}/{version}/system/config_backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemConfigBackup(body, callback)</td>
    <td style="padding:15px">POST_system-config_backup</td>
    <td style="padding:15px">{base_path}/{version}/system/config_backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemConfigRestore(body, callback)</td>
    <td style="padding:15px">POST_system-config_restore</td>
    <td style="padding:15px">{base_path}/{version}/system/config_restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemUploadRestore(file, password, callback)</td>
    <td style="padding:15px">POST_system-upload_restore</td>
    <td style="padding:15px">{base_path}/{version}/system/upload_restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsNodesConnectionSummary(callback)</td>
    <td style="padding:15px">GET_stats-nodes-connection_summary</td>
    <td style="padding:15px">{base_path}/{version}/stats/nodes/connection_summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsNodesCellularHealthSummary(callback)</td>
    <td style="padding:15px">GET_stats-nodes-cellular_health_summary</td>
    <td style="padding:15px">{base_path}/{version}/stats/nodes/cellular_health_summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupportReport(callback)</td>
    <td style="padding:15px">GET_support_report</td>
    <td style="padding:15px">{base_path}/{version}/support_report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuth(callback)</td>
    <td style="padding:15px">GET_auth</td>
    <td style="padding:15px">{base_path}/{version}/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAuth(body, callback)</td>
    <td style="padding:15px">PUT_auth</td>
    <td style="padding:15px">{base_path}/{version}/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBundles(perPage, page, callback)</td>
    <td style="padding:15px">getBundles</td>
    <td style="padding:15px">{base_path}/{version}/bundles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBundle(body, callback)</td>
    <td style="padding:15px">addBundle</td>
    <td style="padding:15px">{base_path}/{version}/bundles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findBundleByID(id, callback)</td>
    <td style="padding:15px">findBundleByID</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBundle(id, body, callback)</td>
    <td style="padding:15px">putBundle</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBundleTags(id, callback)</td>
    <td style="padding:15px">getBundleTags</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}/automatic_tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBundleTag(id, body, callback)</td>
    <td style="padding:15px">createBundleTag</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}/automatic_tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBundlesIdAutomaticTagsTagValueId(tagValueId, id, callback)</td>
    <td style="padding:15px">GET_bundles-id-automatic_tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}/automatic_tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBundlesIdAutomaticTagsTagValueId(tagValueId, id, callback)</td>
    <td style="padding:15px">DELETE_bundles-id-automatic_tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}/automatic_tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBundlesIdAutomaticTagsTagValueId(tagValueId, id, body, callback)</td>
    <td style="padding:15px">PUT_bundles-id-automatic_tags-tag_value_id</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}/automatic_tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadBundleManifest(id, callback)</td>
    <td style="padding:15px">downloadBundleManifest</td>
    <td style="padding:15px">{base_path}/{version}/bundles/{pathv1}/manifest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">GET_users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsers(body, callback)</td>
    <td style="padding:15px">POST_users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupResponse(id, callback)</td>
    <td style="padding:15px">getGroupResponse</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersId(id, body, callback)</td>
    <td style="padding:15px">PUT_users-id</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersId(id, callback)</td>
    <td style="padding:15px">DELETE_users-id</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersPasswordPolicy(callback)</td>
    <td style="padding:15px">GET_users-password_policy</td>
    <td style="padding:15px">{base_path}/{version}/users/password_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersPasswordPolicy(body, callback)</td>
    <td style="padding:15px">PUT_users-password_policy</td>
    <td style="padding:15px">{base_path}/{version}/users/password_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersPasswordReset(body, callback)</td>
    <td style="padding:15px">PUT_users-password_reset</td>
    <td style="padding:15px">{base_path}/{version}/users/password_reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersPasswordValidation(body, callback)</td>
    <td style="padding:15px">PUT_users-password_validation</td>
    <td style="padding:15px">{base_path}/{version}/users/password_validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplates(callback)</td>
    <td style="padding:15px">GET_templates</td>
    <td style="padding:15px">{base_path}/{version}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTemplates(body, callback)</td>
    <td style="padding:15px">POST_templates</td>
    <td style="padding:15px">{base_path}/{version}/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatesId(id, callback)</td>
    <td style="padding:15px">GET_templates-id</td>
    <td style="padding:15px">{base_path}/{version}/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTemplatesId(id, body, callback)</td>
    <td style="padding:15px">PUT_templates-id</td>
    <td style="padding:15px">{base_path}/{version}/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplatesId(id, callback)</td>
    <td style="padding:15px">DELETE_templates-id</td>
    <td style="padding:15px">{base_path}/{version}/templates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplatePush(callback)</td>
    <td style="padding:15px">GET_template_push</td>
    <td style="padding:15px">{base_path}/{version}/template_push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTemplatePush(body, callback)</td>
    <td style="padding:15px">POST_template_push</td>
    <td style="padding:15px">{base_path}/{version}/template_push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTemplatePushIdStage(id, body, callback)</td>
    <td style="padding:15px">PUT_template_push-id-stage</td>
    <td style="padding:15px">{base_path}/{version}/template_push/{pathv1}/stage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipleInstanceDependentLighthouses(callback)</td>
    <td style="padding:15px">GET_multiple_instance-dependent_lighthouses</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/dependent_lighthouses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMultipleInstanceDependentLighthouses(body, callback)</td>
    <td style="padding:15px">POST_multiple_instance-dependent_lighthouses</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/dependent_lighthouses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipleInstanceDependentLighthousesId(id, callback)</td>
    <td style="padding:15px">GET_multiple_instance-dependent_lighthouses-id</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/dependent_lighthouses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMultipleInstanceDependentLighthousesId(id, body, callback)</td>
    <td style="padding:15px">PUT_multiple_instance-dependent_lighthouses-id</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/dependent_lighthouses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMultipleInstanceDependentLighthousesId(id, callback)</td>
    <td style="padding:15px">DELETE_multiple_instance-dependent_lighthouses-id</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/dependent_lighthouses/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMultipleInstanceLhvpn(callback)</td>
    <td style="padding:15px">GET_multiple_instance-lhvpn</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/lhvpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMultipleInstanceLhvpn(body, callback)</td>
    <td style="padding:15px">PUT_multiple_instance-lhvpn</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/lhvpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMultipleInstanceRegistration(body, callback)</td>
    <td style="padding:15px">POST_multiple_instance-registration</td>
    <td style="padding:15px">{base_path}/{version}/multiple_instance/registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetopsModules(callback)</td>
    <td style="padding:15px">GET_netops-modules</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetopsModulesId(id, body, callback)</td>
    <td style="padding:15px">PUT_netops-modules-id</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetopsModulesIdRedeploy(id, callback)</td>
    <td style="padding:15px">GET_netops-modules-id-redeploy</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules/{pathv1}/redeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetopsModulesIdNodesNodeId(id, nodeId, callback)</td>
    <td style="padding:15px">DELETE_netops-modules-id-nodes-node_id</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetopsModulesLicense(body, callback)</td>
    <td style="padding:15px">POST_netops-modules-license</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetopsModulesStatus(callback)</td>
    <td style="padding:15px">GET_netops-modules-status</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetopsModulesSync(callback)</td>
    <td style="padding:15px">GET_netops-modules-sync</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetopsModulesSyncStatus(callback)</td>
    <td style="padding:15px">GET_netops-modules-sync-status</td>
    <td style="padding:15px">{base_path}/{version}/netops/modules/sync/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
