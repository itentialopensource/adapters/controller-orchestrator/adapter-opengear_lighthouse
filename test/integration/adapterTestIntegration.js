/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-opengear_lighthouse',
      type: 'OpengearLighthouse',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const OpengearLighthouse = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Opengear_lighthouse Adapter Test', () => {
  describe('OpengearLighthouse Class Tests', () => {
    const a = new OpengearLighthouse(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const sessionsCreateSessionBodyParam = {
      username: 'root',
      password: samProps.authentication.password
    };
    describe('#createSession - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSession(sessionsCreateSessionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('authenticated', data.response.state);
                assert.equal(false, data.response.password_expired);
                assert.equal('71dcba707b6c177644ede1b224f69096', data.response.session);
                assert.equal('root', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sessions', 'createSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sessionsSessionUid = 'fakedata';
    const sessionsResponseToSessionChallengeBodyParam = {
      session: 'string',
      state: {},
      challenge: 'string'
    };
    describe('#responseToSessionChallenge - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.responseToSessionChallenge(sessionsSessionUid, sessionsResponseToSessionChallengeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sessions', 'responseToSessionChallenge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSession - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSession(sessionsSessionUid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('authenticated', data.response.state);
                assert.equal(false, data.response.password_expired);
                assert.equal('71dcba707b6c177644ede1b224f69096', data.response.session);
                assert.equal('root', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sessions', 'getSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesEnrollNewNodeBodyParam = {
      enrollment: {}
    };
    describe('#enrollNewNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enrollNewNode(nodesEnrollNewNodeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.node);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'enrollNewNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesCreateNodeSmartgroupBodyParam = {};
    describe('#createNodeSmartgroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNodeSmartgroup(nodesCreateNodeSmartgroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.smartgroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'createNodeSmartgroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesId = 'fakedata';
    const nodesCreateNodeTagBodyParam = {
      tag: {
        name: 'Location',
        value: 'New York'
      }
    };
    describe('#createNodeTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNodeTag(nodesId, nodesCreateNodeTagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'createNodeTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodes(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.nodes));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getnodesearchfields1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getnodesearchfields1((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.fields));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getnodesearchfields1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getnodesearchfields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getnodesearchfields(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.nodes));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getnodesearchfields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadManifest((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'downloadManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeSmartgroupList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeSmartgroupList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal(true, Array.isArray(data.response.smartgroups));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodeSmartgroupList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesGroupId = 'fakedata';
    const nodesUpdateNodeSmartgroupBodyParam = {};
    describe('#updateNodeSmartgroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNodeSmartgroup(nodesGroupId, nodesUpdateNodeSmartgroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'updateNodeSmartgroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeSmartgroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeSmartgroup(nodesGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.smartgroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodeSmartgroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmartgroupNodes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSmartgroupNodes(nodesGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getSmartgroupNodes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesPutNodeBodyParam = {
      node: {
        name: 'test1',
        approved: false,
        connection_info: {
          address: '192.168.70.23',
          username: 'new_username',
          password: samProps.authentication.password
        },
        tags: [
          {
            id: 'nodes_tags-4',
            name: 'Location',
            value: 'Hong Kong'
          }
        ]
      }
    };
    describe('#putNode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putNode(nodesId, nodesPutNodeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'putNode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findNodeByID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findNodeByID(nodesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.node);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'findNodeByID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeCellHealthRuntimeDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeCellHealthRuntimeDetails(nodesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.node_cellhealth_runtime_details);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodeCellHealthRuntimeDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodesIdPorts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNodesIdPorts(nodesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodesIdPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeRegistrationPackageByID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNodeRegistrationPackageByID(nodesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodeRegistrationPackageByID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodeTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodeTags(nodesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodeTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nodesTagValueId = 'fakedata';
    const nodesPutNodesIdTagsTagValueIdBodyParam = {
      tag: {
        name: 'Location',
        value: 'USA.NewYork'
      }
    };
    describe('#putNodesIdTagsTagValueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putNodesIdTagsTagValueId(nodesTagValueId, nodesId, nodesPutNodesIdTagsTagValueIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'putNodesIdTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodesIdTagsTagValueId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodesIdTagsTagValueId(nodesTagValueId, nodesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.example1);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'getNodesIdTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portsPostPortsSmartgroupsBodyParam = {};
    describe('#postPortsSmartgroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPortsSmartgroups(portsPostPortsSmartgroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.smartgroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'postPortsSmartgroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPorts(null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.ports));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'getPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortsSmartgroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortsSmartgroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.meta);
                assert.equal(true, Array.isArray(data.response.smartgroups));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'getPortsSmartgroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portsId = 'fakedata';
    const portsPutPortsSmartgroupsIdBodyParam = {};
    describe('#putPortsSmartgroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putPortsSmartgroupsId(portsId, portsPutPortsSmartgroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'putPortsSmartgroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortsSmartgroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortsSmartgroupsId(portsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.smartgroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'getPortsSmartgroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPortsId(portsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'getPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getManagedDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getManagedDevices(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.nodes));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ManagedDevices', 'getManagedDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSearchId2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSearchId2(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.search);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Search', 'getSearchId2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSearchId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSearchId(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.search);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Search', 'getSearchId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSearchId1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSearchId1(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.search);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Search', 'getSearchId1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPostServicesSyslogBodyParam = {};
    describe('#postServicesSyslog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postServicesSyslog(servicesPostServicesSyslogBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.syslogServer);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'postServicesSyslog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesCellhealthBodyParam = {};
    describe('#putServicesCellhealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putServicesCellhealth(servicesPutServicesCellhealthBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesCellhealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesCellhealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesCellhealth((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.cellhealth);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesCellhealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesConsoleGatewayBodyParam = {};
    describe('#putServicesConsoleGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putServicesConsoleGateway(servicesPutServicesConsoleGatewayBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesConsoleGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesConsoleGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesConsoleGateway((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.console_gateway);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesConsoleGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesHttpsBodyParam = {};
    describe('#putServicesHttps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putServicesHttps(servicesPutServicesHttpsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesHttps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesHttps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesHttps(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.https);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesHttps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesLhvpnBodyParam = {
      lhvpn: {
        mtu: 1400,
        address: '192.168.128.0',
        mask: '255.255.224.0',
        cidr: 19
      }
    };
    describe('#putServicesLhvpn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putServicesLhvpn(servicesPutServicesLhvpnBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesLhvpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesLhvpn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServicesLhvpn((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesLhvpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesNtpBodyParam = {};
    describe('#putServicesNtp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putServicesNtp(servicesPutServicesNtpBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesNtp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesNtp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesNtp((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ntp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesNtp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesSnmpBodyParam = {
      snmp: {
        enabled: true,
        protocol: 'UDP',
        read_write_community: 'secret',
        read_only_community: 'secret',
        auth_protocol: 'SHA',
        auth_password: samProps.authentication.password,
        username: 'michaelf',
        engine_id: '0x80001f8803555000000000',
        v3_enabled: true,
        v1_enabled: false,
        privacy_protocol: 'DES',
        privacy_password: samProps.authentication.password,
        security_level: 'noAuthNoPriv',
        location: 'BNE',
        contact: 'mail@example.com'
      }
    };
    describe('#putServicesSnmp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putServicesSnmp(servicesPutServicesSnmpBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesSnmp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesSnmp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServicesSnmp((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesSnmp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesPutServicesSnmpManagerBodyParam = {
      snmp_manager: {
        enabled: true,
        protocol: 'UDP',
        community: 'secret',
        auth_protocol: 'SHA',
        auth_password: samProps.authentication.password,
        username: 'michaelf',
        engine_id: '0x80001f8803555000000000',
        version: 'v3',
        privacy_protocol: 'DES',
        privacy_password: samProps.authentication.password,
        security_level: 'noAuthNoPriv',
        msg_type: 'TRAP',
        address: 'snmp.example.com',
        port: 167,
        traps: [
          'nodes_conn_status',
          'nodes_cellular_health_status'
        ]
      }
    };
    describe('#putServicesSnmpManager - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putServicesSnmpManager(servicesPutServicesSnmpManagerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesSnmpManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesSnmpManager - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServicesSnmpManager((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesSnmpManager', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesSyslog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesSyslog((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.syslogServers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesSyslog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const servicesSyslogServerId = 'fakedata';
    const servicesPutServicesSyslogSyslogServerIdBodyParam = {};
    describe('#putServicesSyslogSyslogServerId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putServicesSyslogSyslogServerId(servicesSyslogServerId, servicesPutServicesSyslogSyslogServerIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'putServicesSyslogSyslogServerId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesSyslogSyslogServerId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesSyslogSyslogServerId(servicesSyslogServerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.syslogServer);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'getServicesSyslogSyslogServerId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsPostTagsNodeTagsBodyParam = {
      nodeTag: {
        name: 'Location',
        values: [
          {
            value: 'USA.NewYork'
          },
          {
            value: 'UK.London'
          }
        ]
      }
    };
    describe('#postTagsNodeTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTagsNodeTags(tagsPostTagsNodeTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.nodeTag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'postTagsNodeTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTagsNodeTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTagsNodeTags(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.nodeTags));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'getTagsNodeTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tagsTagValueId = 'fakedata';
    const tagsPutTagsNodeTagsTagValueIdBodyParam = {
      nodeTag: {
        name: 'Location',
        values: [
          {
            id: 'tags_node_tags_values_90',
            value: 'USA.NewYork'
          },
          {
            id: 'tags_node_tags_values_91',
            value: 'UK.London'
          },
          {
            value: 'France.Paris'
          }
        ]
      }
    };
    describe('#putTagsNodeTagsTagValueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTagsNodeTagsTagValueId(tagsTagValueId, tagsPutTagsNodeTagsTagValueIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'putTagsNodeTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaces(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'getInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesId = 'fakedata';
    const interfacesPutInterfacesIdBodyParam = {
      interface: {
        enabled: true,
        description: 'Default IPv4 Static Address',
        ipv4_static_settings: {
          netmask: '255.255.255.0',
          address: '192.168.0.8'
        },
        physif: 'net1',
        role: 'lan',
        name: 'default-conn-1',
        mode: 'static',
        media: 'auto'
      }
    };
    describe('#putInterfacesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putInterfacesId(interfacesId, interfacesPutInterfacesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'putInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfacesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfacesId(interfacesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.interface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'getInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPostSystemConfigBackupBodyParam = {
      password: samProps.authentication.password,
      user_files: [
        'string'
      ]
    };
    describe('#postSystemConfigBackup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemConfigBackup(systemPostSystemConfigBackupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_config_backup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'postSystemConfigBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPostSystemConfigRestoreBodyParam = {
      id: 'string'
    };
    describe('#postSystemConfigRestore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemConfigRestore(systemPostSystemConfigRestoreBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_config_restore);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'postSystemConfigRestore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPostSystemExternalEndpointsBodyParam = {};
    describe('#postSystemExternalEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemExternalEndpoints(systemPostSystemExternalEndpointsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_external_endpoint);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'postSystemExternalEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemFile = 'fakedata';
    // describe('#postSystemFirmwareUpgrade - errors', () => {
    //   it('should work if integrated or standalone with mockdata', (done) => {
    //     try {
    //       a.postSystemFirmwareUpgrade(systemFile, null, null, (data, error) => {
    //         try {
    //           if (stub) {
    //             runCommonAsserts(data, error);
    //             assert.equal('object', typeof data.response.system_firmware_upgrade_status);
    //           } else {
    //             runCommonAsserts(data, error);
    //           }
    //           saveMockData('System', 'postSystemFirmwareUpgrade', 'default', data);
    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    const systemPostSystemLicensesBodyParam = {
      licenses: [
        {
          raw: 'eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1pY2hhZWxmQGZpdHp5c2l0ZS5jb20iLCJzdWIiOiJ...'
        },
        {
          raw: 'eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1pY2hhZWxmQGZpdHp5c2l0ZS5jb20iLCJzdWIiOiJ...'
        }
      ]
    };
    describe('#postSystemLicenses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSystemLicenses(systemPostSystemLicensesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'postSystemLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSystemLicensesFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSystemLicensesFile(systemFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'postSystemLicensesFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSystemUploadRestore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemUploadRestore(systemFile, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_upload_restore);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'postSystemUploadRestore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemAlternateApiBodyParam = {
      system_alternate_api: {
        enabled: true,
        port: 8999
      }
    };
    describe('#putSystemAlternateApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemAlternateApi(systemPutSystemAlternateApiBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemAlternateApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemAlternateApi - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemAlternateApi((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_alternate_api);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemAlternateApi', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemCliSessionTimeoutBodyParam = {
      system_cli_session_timeout: {
        timeout: 10
      }
    };
    describe('#putSystemCliSessionTimeout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemCliSessionTimeout(systemPutSystemCliSessionTimeoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemCliSessionTimeout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemCliSessionTimeout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemCliSessionTimeout((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_cli_session_timeout);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemCliSessionTimeout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemId = 'fakedata';
    describe('#getSystemConfigBackup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemConfigBackup(systemId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemConfigBackup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemEntitlements - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemEntitlements((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemEntitlements', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemExternalEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemExternalEndpoints((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.system_external_endpoints));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemExternalEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemExternalEndpointsIdBodyParam = {};
    describe('#putSystemExternalEndpointsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemExternalEndpointsId(systemId, systemPutSystemExternalEndpointsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemExternalEndpointsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemExternalEndpointsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemExternalEndpointsId(systemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_external_endpoint);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemExternalEndpointsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemFirmwareUpgradeStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemFirmwareUpgradeStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_firmware_upgrade_status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemFirmwareUpgradeStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemGlobalEnrollmentTokenBodyParam = {
      system_global_enrollment_token: {
        token: 'mySomewhatSecret'
      }
    };
    describe('#putSystemGlobalEnrollmentToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemGlobalEnrollmentToken(systemPutSystemGlobalEnrollmentTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemGlobalEnrollmentToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemGlobalEnrollmentToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemGlobalEnrollmentToken((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_global_enrollment_token);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemGlobalEnrollmentToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemHostnameBodyParam = {
      system_hostname: {
        hostname: 'Lighthouse'
      }
    };
    describe('#putSystemHostname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemHostname(systemPutSystemHostnameBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemHostname', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemHostname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemHostname((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_hostname);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemHostname', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemLicenses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSystemLicenses((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemLighthouseName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemLighthouseName((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemLighthouseName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemManifestLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemManifestLink((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_global_manifest_link);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemManifestLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemOsDefaultAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemOsDefaultAddress((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.os_default_external_address);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemOsDefaultAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemSshPortBodyParam = {
      system_ssh_port: {
        port: 22
      }
    };
    describe('#putSystemSshPort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemSshPort(systemPutSystemSshPortBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemSshPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemSshPort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemSshPort((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_ssh_port);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemSshPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemTimeBodyParam = {
      time: {
        time: '16:22 Sep 23, 2016'
      }
    };
    describe('#putSystemTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemTime(systemPutSystemTimeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemTime((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.time);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemTimezoneBodyParam = {
      system_timezone: {
        timezone: 'Australia/Brisbane'
      }
    };
    describe('#putSystemTimezone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemTimezone(systemPutSystemTimezoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemTimezone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemTimezone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemTimezone((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemTimezone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemPutSystemWebuiSessionTimeoutBodyParam = {
      system_webui_session_timeout: {
        timeout: 20
      }
    };
    describe('#putSystemWebuiSessionTimeout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSystemWebuiSessionTimeout(systemPutSystemWebuiSessionTimeoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'putSystemWebuiSessionTimeout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemWebuiSessionTimeout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemWebuiSessionTimeout((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.system_webui_session_timeout);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemWebuiSessionTimeout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsNodesCellularHealthSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsNodesCellularHealthSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connectionSummary));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsNodesCellularHealthSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsNodesConnectionSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsNodesConnectionSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connectionSummary));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsNodesConnectionSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSupportReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSupportReport((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.support_report);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SupportReport', 'getSupportReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authPutAuthBodyParam = {};
    describe('#putAuth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putAuth(authPutAuthBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'putAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuth((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.auth);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getAuth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bundlesAddBundleBodyParam = {};
    describe('#addBundle - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addBundle(bundlesAddBundleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.bundle);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'addBundle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bundlesId = 'fakedata';
    const bundlesCreateBundleTagBodyParam = {};
    describe('#createBundleTag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createBundleTag(bundlesId, bundlesCreateBundleTagBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'createBundleTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBundles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBundles(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.bundles));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'getBundles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bundlesPutBundleBodyParam = {};
    describe('#putBundle - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putBundle(bundlesId, bundlesPutBundleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'putBundle', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findBundleByID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.findBundleByID(bundlesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.bundle);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'findBundleByID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBundleTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBundleTags(bundlesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'getBundleTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bundlesTagValueId = 'fakedata';
    const bundlesPutBundlesIdAutomaticTagsTagValueIdBodyParam = {};
    describe('#putBundlesIdAutomaticTagsTagValueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBundlesIdAutomaticTagsTagValueId(bundlesTagValueId, bundlesId, bundlesPutBundlesIdAutomaticTagsTagValueIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'putBundlesIdAutomaticTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBundlesIdAutomaticTagsTagValueId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBundlesIdAutomaticTagsTagValueId(bundlesTagValueId, bundlesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.tag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'getBundlesIdAutomaticTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadBundleManifest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadBundleManifest(bundlesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'downloadBundleManifest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPostUsersBodyParam = {
      user: {
        username: 'joe',
        password: samProps.authentication.password,
        description: 'local user',
        enabled: true,
        expired: false,
        locked_out: false,
        no_password: false,
        groups: [
          'groups-1'
        ]
      }
    };
    describe('#postUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postUsers(usersPostUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUsers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPutUsersPasswordPolicyBodyParam = {
      password_policy: {
        enabled: true,
        min_password_length: 6,
        no_username: false,
        must_contain_capital_letter: true,
        must_contain_number: false,
        must_contain_special_character: true,
        password_reuse_period: -1,
        password_expiry_period: 30,
        max_login_attempts: 5,
        login_lockout_period: 5
      }
    };
    describe('#putUsersPasswordPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putUsersPasswordPolicy(usersPutUsersPasswordPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersPasswordPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersPasswordPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUsersPasswordPolicy((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersPasswordPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPutUsersPasswordResetBodyParam = {
      current_password: samProps.authentication.password,
      new_password: samProps.authentication.password
    };
    describe('#putUsersPasswordReset - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putUsersPasswordReset(usersPutUsersPasswordResetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersPasswordReset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPutUsersPasswordValidationBodyParam = {
      password_validation: {
        username: 'joe',
        password: samProps.authentication.password
      }
    };
    describe('#putUsersPasswordValidation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putUsersPasswordValidation(usersPutUsersPasswordValidationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersPasswordValidation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersId = 'fakedata';
    const usersPutUsersIdBodyParam = {
      group: {}
    };
    describe('#putUsersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putUsersId(usersId, usersPutUsersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'putUsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupResponse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupResponse(usersId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.group);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getGroupResponse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templatesPostTemplatesBodyParam = {
      template: {}
    };
    describe('#postTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTemplates(templatesPostTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Templates', 'postTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplates((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Templates', 'getTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templatesId = 'fakedata';
    const templatesPutTemplatesIdBodyParam = {
      template: {
        type: 'auth',
        name: 'food',
        authMode: 'radius',
        radiusPassword: 'bork',
        radiusAuthenticationServers: [
          {
            hostname: 'bar'
          }
        ],
        radiusAccountingServers: [
          {
            hostname: 'baz'
          }
        ]
      }
    };
    describe('#putTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTemplatesId(templatesId, templatesPutTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Templates', 'putTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplatesId(templatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Templates', 'getTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templatePushPostTemplatePushBodyParam = {
      template_push: {
        template_ids: [
          'templates-1'
        ],
        node_ids: [
          'nodes-1',
          'nodes-2'
        ]
      }
    };
    describe('#postTemplatePush - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTemplatePush(templatePushPostTemplatePushBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplatePush', 'postTemplatePush', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplatePush - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTemplatePush((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplatePush', 'getTemplatePush', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templatePushId = 'fakedata';
    const templatePushPutTemplatePushIdStageBodyParam = {
      template_push: {
        stage: 'preflight',
        node_ids: [
          'nodes-1',
          'nodes-2'
        ]
      }
    };
    describe('#putTemplatePushIdStage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putTemplatePushIdStage(templatePushId, templatePushPutTemplatePushIdStageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TemplatePush', 'putTemplatePushIdStage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multipleInstancePostMultipleInstanceDependentLighthousesBodyParam = {
      dependent_lighthouse: {
        address: '192.168.73.100',
        username: 'usera',
        password: samProps.authentication.password,
        lhvpn_subnet_netmask: '255.255.255.0',
        lhvpn_subnet_address: '172.16.4.0'
      }
    };
    describe('#postMultipleInstanceDependentLighthouses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMultipleInstanceDependentLighthouses(multipleInstancePostMultipleInstanceDependentLighthousesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.dependent_lighthouse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'postMultipleInstanceDependentLighthouses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multipleInstancePostMultipleInstanceRegistrationBodyParam = {
      instance_id: 2,
      subnet: {
        address: '172.16.2.1',
        netmask: '255.255.255.0'
      },
      master_address: '192.168.20.3',
      vpn_port: 1195,
      instance_network_port: 443,
      ca: '-----BEGIN CERTIFICATE-----...-----END CERTIFICATE-----',
      key: '-----BEGIN PRIVATE KEY-----...',
      cert: '-----BEGIN CERTIFICATE-----...-----END CERTIFICATE-----'
    };
    describe('#postMultipleInstanceRegistration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMultipleInstanceRegistration(multipleInstancePostMultipleInstanceRegistrationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('configured', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'postMultipleInstanceRegistration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipleInstanceDependentLighthouses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMultipleInstanceDependentLighthouses((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.dependent_lighthouses));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'getMultipleInstanceDependentLighthouses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multipleInstanceId = 'fakedata';
    const multipleInstancePutMultipleInstanceDependentLighthousesIdBodyParam = {
      dependent_lighthouse: {
        lhvpn_subnet_address: '172.16.3.128',
        lhvpn_subnet_netmask: '255.255.255.128'
      }
    };
    describe('#putMultipleInstanceDependentLighthousesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putMultipleInstanceDependentLighthousesId(multipleInstanceId, multipleInstancePutMultipleInstanceDependentLighthousesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'putMultipleInstanceDependentLighthousesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipleInstanceDependentLighthousesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMultipleInstanceDependentLighthousesId(multipleInstanceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.dependent_lighthouse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'getMultipleInstanceDependentLighthousesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multipleInstancePutMultipleInstanceLhvpnBodyParam = {
      multiple_instance_lhvpn: {
        mask: '255.255.255.0',
        address: '172.16.1.0'
      }
    };
    describe('#putMultipleInstanceLhvpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putMultipleInstanceLhvpn(multipleInstancePutMultipleInstanceLhvpnBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'putMultipleInstanceLhvpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMultipleInstanceLhvpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMultipleInstanceLhvpn((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.multiple_instance_lhvpn);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'getMultipleInstanceLhvpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netopsPostNetopsModulesLicenseBodyParam = {
      sku: 'nom-prov'
    };
    describe('#postNetopsModulesLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNetopsModulesLicense(netopsPostNetopsModulesLicenseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'postNetopsModulesLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetopsModules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetopsModules((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'getNetopsModules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetopsModulesStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetopsModulesStatus((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'getNetopsModulesStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetopsModulesSync - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetopsModulesSync((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'getNetopsModulesSync', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetopsModulesSyncStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetopsModulesSyncStatus((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'getNetopsModulesSyncStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netopsId = 'fakedata';
    const netopsPutNetopsModulesIdBodyParam = {
      netops_modules: {
        always_activate: 0
      }
    };
    describe('#putNetopsModulesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putNetopsModulesId(netopsId, netopsPutNetopsModulesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'putNetopsModulesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetopsModulesIdRedeploy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetopsModulesIdRedeploy(netopsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'getNetopsModulesIdRedeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSession - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSession(sessionsSessionUid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sessions', 'deleteSession', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodeSmartgroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNodeSmartgroup(nodesGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'deleteNodeSmartgroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNodesId(nodesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'deleteNodesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodesIdTagsTagValueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNodesIdTagsTagValueId(nodesTagValueId, nodesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nodes', 'deleteNodesIdTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePortsSmartgroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePortsSmartgroupsId(portsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'deletePortsSmartgroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicesSyslogSyslogServerId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServicesSyslogSyslogServerId(servicesSyslogServerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Services', 'deleteServicesSyslogSyslogServerId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTagsNodeTagsTagValueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTagsNodeTagsTagValueId(tagsTagValueId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tags', 'deleteTagsNodeTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSystemConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSystemConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'deleteSystemConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSystemExternalEndpointsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSystemExternalEndpointsId(systemId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'deleteSystemExternalEndpointsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBundlesIdAutomaticTagsTagValueId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBundlesIdAutomaticTagsTagValueId(bundlesTagValueId, bundlesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bundles', 'deleteBundlesIdAutomaticTagsTagValueId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersId(usersId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplatesId(templatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Templates', 'deleteTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleInstanceDependentLighthousesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleInstanceDependentLighthousesId(multipleInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MultipleInstance', 'deleteMultipleInstanceDependentLighthousesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netopsNodeId = 'fakedata';
    describe('#deleteNetopsModulesIdNodesNodeId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetopsModulesIdNodesNodeId(netopsId, netopsNodeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-opengear_lighthouse-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Netops', 'deleteNetopsModulesIdNodesNodeId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
